package kprod;

/**
 *
 * @author soujisama
 */
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.kinesis.producer.KinesisProducer;
import com.amazonaws.services.kinesis.producer.KinesisProducerConfiguration;
import com.amazonaws.services.kinesis.producer.UserRecordResult;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import java.nio.ByteBuffer;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;
import kprod.model.EventMessage;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProducerController {

    private final String streamName = "TEST_STREAM";
    private final String partitionKey = "PARTITION_ONE";
    private final String accessKeyId = "AKIAJUZA35BXPCBJ6PDQ";
    private final String secretKeyId = "PlEG6rcPa06RB61F0m1m9VOWBBWM9K5hvX2XvJGK";
    private final String region = "ap-southeast-1";
    ExecutorService executor = Executors.newCachedThreadPool();

    BasicAWSCredentials credentials = new BasicAWSCredentials(accessKeyId, secretKeyId);
    
    KinesisProducerConfiguration config = new KinesisProducerConfiguration()
            .setRecordMaxBufferedTime(3000)
            .setMaxConnections(1)
            .setRequestTimeout(60000)
            .setRegion(region)
            .setCredentialsProvider(new AWSStaticCredentialsProvider(credentials));

    private KinesisProducer kinesis = null;

    public ProducerController() {
        
    }
    
    public void generateMessages(int messagesPerSecond) {
        int timeout = 1000/messagesPerSecond;
        
        EventMessage message = EventMessage.createMessageOfType("noise");
        message.addData("empty", UUID.randomUUID().toString());
        
        if(kinesis == null) {
            kinesis = new KinesisProducer(config);
        }
        
        try {
            executor.shutdownNow();
            executor = Executors.newCachedThreadPool();
        }
        catch(RejectedExecutionException e) {
            e.printStackTrace();
        }
        
        Runnable task = () -> {
            try {
                while(true) {
                    writeMessageToStream(message);
                    Thread.sleep(timeout);
                }
            }
            catch(InterruptedException e) {
                e.printStackTrace();
            }
        };
        
        executor.execute(task);
    }
        
    @GetMapping("/")
    public String createForm(Model model) {
        model.addAttribute("eventMessage", new EventMessage());
        return "<html>"
                + "<form action='/setFrequency' method='GET'>"
                + "Enter Number of Messages / Second: <input type='text' name='number_of_messages' /><br />"
                + "<input type='submit' />"
                + "</form>"
                + "</html>";
    }

    
    @RequestMapping("/setFrequency")
    public String setFrequency(@RequestParam(value = "number_of_messages", defaultValue = "1") String messagesPerSecond) {
        
        int number = Integer.parseInt(messagesPerSecond);
        generateMessages(number);

        return "<html>"
                + "<form action='/setFrequency' method='GET'>"
                + "Enter Number of Messages / Second: <input type='text' name='number_of_messages' /><br />"
                + "<input type='submit' />"
                + "</form>"
                + "</html>";

    }
    
    public void writeMessageToStream(EventMessage message) {
        try {
            ByteBuffer bytes = message.convertToByteBuffer();
            ListenableFuture<UserRecordResult> f = kinesis.addUserRecord(streamName, partitionKey, bytes);
            Futures.addCallback(f, new FutureCallback<UserRecordResult>() {

                    @Override
                    public void onSuccess(UserRecordResult v) {
                        System.out.println("Saved noise to stream with sequence number: " + v.getSequenceNumber() + " -> " + message.getMessageId());
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        System.out.println(t.getMessage());
                        System.out.println("Failed to add to stream");
                        Logger.getLogger(ProducerController.class.getName()).log(Level.SEVERE,null,t);
                    }
                }
            );
        } catch (JsonProcessingException ex) {
            Logger.getLogger(ProducerController.class.getName()).log(Level.SEVERE, "Could not convert Message to Stream data", ex);
        }
    }
}
