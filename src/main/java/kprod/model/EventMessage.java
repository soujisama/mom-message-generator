package kprod.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 *
 * @author soujisama
 */
public class EventMessage {
    public final String messageId;
    public final long produceTime;
    public Map<String,String> tags;
    public Map<String, String> data;
    private static final String MESSAGE_TYPE_TAG = "event_name";
    
    public EventMessage() {
        this.messageId = UUID.randomUUID().toString();
        this.produceTime = System.currentTimeMillis();
        tags = new HashMap<String,String>();
        data = new HashMap<String,String>();
    }    
    
    public static EventMessage createMessageOfType(String messageType) {
        EventMessage eventMessage = new EventMessage();
        eventMessage.setType(messageType);
        
        return eventMessage;
    }
    
    public static EventMessage createMessageFromByteBuffer(ByteBuffer data) throws IOException {
        ByteBuffer datax = ByteBuffer.wrap(data.array());
        ObjectMapper mapper = new ObjectMapper();
        EventMessage eventMessage = mapper.readValue(datax.array(), EventMessage.class);
             
        return eventMessage;
    }
    
    public ByteBuffer convertToByteBuffer() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        byte[] bytes = mapper.writeValueAsBytes(this);
        ByteBuffer buffer = ByteBuffer.wrap(bytes);
        
        return buffer;
    }
    
    public String getMessageId() {
        return messageId;
    }
    
    private void setType(String type) {
        tags.put(MESSAGE_TYPE_TAG,type);
    }
    
    
    public long getProduceTime() {
        return produceTime;
    }
    
    public void addTag(String key, String value) throws Exception {
        if(key.equals(MESSAGE_TYPE_TAG)) {
            throw new Exception();
        }
        tags.put(key,value);
    }
    
    public void addData(String key, String value) {
        data.put(key,value);
    }
    
    public String getData(String key) {
        return data.get(key);
    }

    public String getTag(String key) {
        return tags.get(key);
    }
    
    public String getType() {
        return tags.get(MESSAGE_TYPE_TAG);
    }
    
    @Override
    public String toString() {
        String string;
        
        try {
            ObjectMapper mapper = new ObjectMapper();
            string = mapper.writeValueAsString(this);
        }
        catch(JsonProcessingException e) {
            string = "error converting to string";
        }
        
        return string;
    }
    

}
