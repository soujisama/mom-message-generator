package kprod;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.kinesis.producer.KinesisProducer;
import com.amazonaws.services.kinesis.producer.KinesisProducerConfiguration;
import kprod.ProducerController;
import kprod.model.EventMessage;
import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author soujisama
 */
public class ProducerControllerTest {

    private final ProducerController instance = new ProducerController();

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testGenerateEventMessageTest() {

        String messageType = "new_order";
        
        String clientId = "1";
        String orderId = "2";
        
        String clientIdTagName = "client_id";
        String orderIdTagName = "order_id";

        EventMessage eventMessage = EventMessage.createMessageOfType(messageType);
        eventMessage.addData(clientIdTagName, clientId);
        eventMessage.addData(orderIdTagName, orderId);

        assertTrue(eventMessage.getMessageId().length() > 0);
        assertEquals(messageType,eventMessage.getType());
        assertEquals(clientId, eventMessage.getData(clientIdTagName));
        assertEquals(orderId, eventMessage.getData(orderIdTagName));
    }

    @Test
    public void testKinesis() {

        final String accessKeyId = "AKIAJUZA35BXPCBJ6PDQ";
        final String secretKeyId = "PlEG6rcPa06RB61F0m1m9VOWBBWM9K5hvX2XvJGK";

        BasicAWSCredentials credentials = new BasicAWSCredentials(accessKeyId, secretKeyId);

        KinesisProducerConfiguration config = new KinesisProducerConfiguration()
                .setRecordMaxBufferedTime(3000)
                .setMaxConnections(1)
                .setRequestTimeout(60000)
                .setRegion("ap-southeast-1")
                .setCredentialsProvider(new AWSStaticCredentialsProvider(credentials));

        final KinesisProducer kinesis = new KinesisProducer(config);
        System.out.println("LOL");
    }
}
